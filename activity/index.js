// Create a function which will be able to add two numbers

function sum(add_1, add_2){
	let sum = add_1 + add_2;
	console.log("The sum of " + add_1 + " and " + add_2 +":");
	console.log(sum);
};

// Create a function which will be able to subtract two numbers

function difference(minuend, subtrahend){
	let diff = minuend - subtrahend;
	console.log("The difference of " + minuend +" and " + subtrahend +":");
	console.log(diff)

};


// Invoke and pass 2 arguments to the addition function
sum(5,15);

// Invoke and pass 2 arguments to the subraction function
difference(20,5);

// Create a function which will be able to multiply two numbers

function mult(factor_1, factor_2){
	let product = factor_1 * factor_2;
	console.log("The product of " + factor_1 + " and " + factor_2 +":");
	return product;

};

// Create a function which will be able to divide two numbers

function div(dividend, divisor){
	let quotient = dividend / divisor;
	console.log("The quotient of " + dividend + " and " + divisor +":");
	return quotient;

};

// Create a new variable called product
let product = mult(50, 10);
console.log(product);

// Create a new variable called quotient
let quotient = div(50, 10);
console.log(quotient);

// Create a function that finds the area of a circle
function circle(radius){
	let area = 3.14*(radius**2);
	console.log("The result of getting the area of a circle with "+ radius + " radius" +":");
	return area;
};

// Create a new variable called circleArea
let circleArea = circle(15);
console.log(circleArea);

// Create a function that calculates the average of 4 scores
function average(score_1, score_2, score_3, score_4){
	total = score_1 + score_2 + score_3 + score_4;
	average = total/4;
	console.log("The average of " + score_1 + ",", score_2 + ",", score_3 + "," + " and " + score_4 +":");
	return average;
};

// Create a new variable called averageVar
let averageVar = average(20, 40, 60, 80);
console.log(averageVar);


// Create a function that calculates if a score is passing
function passing(score, total){
	percentage = score/total;
	console.log("Is " + score + "/" + total + " a passing score?"+":");
	return percentage >= .75;
};

// Create a new variable called isPassed
let isPassed = passing(38,50);
console.log(isPassed);